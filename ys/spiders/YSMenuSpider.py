#-*- coding: utf-8 -*-
__author__ = 'onurbaran'
from scrapy.spider import Spider
from scrapy.selector import Selector
from ys.spiders import YSMenuPatterns
from ys.items import YSMenuItem
class RestaurantSpider(Spider):
    name = "ys-menu-spider"
    allowed_domains = ["yemeksepeti.com"]
    start_urls = YSMenuPatterns.URL

    def parse(self, response):
        sel = Selector(response)
        item = YSMenuItem()
        item['location_address'] = sel.css(YSMenuPatterns.ITEM_PATTERNS['location_address']).extract()
        item['arrive_time'] = sel.css(YSMenuPatterns.ITEM_PATTERNS['arrive_time']).extract()
        sites = sel.css(YSMenuPatterns.MAIN_PATTERN)
        items = []
        for site in sites:
            item['food_name'] = site.css(YSMenuPatterns.ITEM_PATTERNS['food_name']).extract()
            item['food_price'] = site.css(YSMenuPatterns.ITEM_PATTERNS['food_price']).extract()
            item['food_description'] = site.css(YSMenuPatterns.ITEM_PATTERNS['food_description']).extract()
            items.append(item)
        return items
