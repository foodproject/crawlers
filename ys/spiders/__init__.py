"""Spider's HTML pattern definitions"""


class YSNewAddedPatterns:
    URL = 'http://istanbul.yemeksepeti.com/TR/Restoranlar/Yeniler/k.ys'
    MAIN_PATTERN = 'div[class*=rmd_item]'

    #@TODO: Generate pattern name from items.py
    ITEM_PATTERNS = {
        'restaurant_name':'::attr(restaurantname)',
        'restaurant_category_id': '::attr(restaurantcategory)',
        'payment_methods': '::attr(paymentmethods)',
        'min_price':'::attr(minprice)',
        'working_hours': '::attr(workinghourstext)',
        'is_open': '::attr(isopen)',
        'local_id': '::attr(oid)',
        'restaurant_link' : '::attr(restaurantlink)',
        'speed_rating' : '::attr(speed)',
        'service_rating' : '::attr(service)',
        'flavour_rating' : '::attr(flavour)'
    }


class YSRestaurantPatterns:
    #generated from location-url crawler
    URL = [
        "http://istanbul.yemeksepeti.com/TR/Restoranlar/Bolge/kadikoy-%28merkez%29/e0b46c42-6718-4c7c-95be-208e835acd6a/tc/k.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoranlar/Bolge/kadikoy-%28bahariye%29/e90d52a0-65e1-463d-9031-ac6b19a1cd69/tc/k.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoranlar/Bolge/acibadem/f0360586-cdbc-4018-a765-5935fb699f74/tc/k.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoranlar/Bolge/kadikoy-%28rasimpasa%29/af6becc6-8a19-44db-b488-a8e742a53d1d/tc/k.ys"
    ]

    MAIN_PATTERN = 'div[class*=rmd_item]'
    ITEM_PATTERNS = {
        'restaurant_name':'::attr(restaurantname)',
        'restaurant_category_id': '::attr(restaurantcategory)',
        'payment_methods': '::attr(paymentmethods)',
        'min_price':'::attr(minprice)',
        'working_hours': '::attr(workinghourstext)',
        'is_open': '::attr(isopen)',
        'local_id': '::attr(oid)',
        'restaurant_link' : '::attr(restaurantlink)',
        'speed_rating' : '::attr(speed)',
        'service_rating' : '::attr(service)',
        'flavour_rating' : '::attr(flavour)'
    }


class YSMenuPatterns:
    #demo amaciyla 8 10 tane url koyduk, normalde url ler listeden gelecek
    URL = [
        "http://istanbul.yemeksepeti.com/TR/Restoran/01-Buyuk-Adana-Kebapcisi_-Merdivenkoy/5343/TR_ISTANBUL/m.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoran/2005-Valibey-Kebap-ve-Kunefe_-Kozyatagi/4097/TR_ISTANBUL/m.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoran/Abbas-Waffle_-Acibadem/c8bc9ffe-a56d-4701-8530-e359642d4459/TR_ISTANBUL/m.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoran/Adana-Durum-ve-Kebap-Evi_-Kadikoy/d9937759-b171-45f4-be21-a99338ac1695/TR_ISTANBUL/m.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoran/Adanali-Hasan-Kolcuoglu_-Ciftehavuzlar/6fe8b232-af40-4a37-98ff-08efe6e814b1/TR_ISTANBUL/m.ys",
        "http://istanbul.yemeksepeti.com/TR/Restoran/Altmis-Uc-Urfa-Durum-Evi_-Kuyubasi/5c502075-e011-4990-859b-60808931e959/TR_ISTANBUL/m.ys",
        "http://istanbul.yemeksepeti.com/TR/Market-Restoran/Antakyakolik/4060e42f-5cf3-45b5-8576-3761d11bf232/Market/i.ys"
    ]

    MAIN_PATTERN = 'div[class*=rmd_product]'
    ITEM_PATTERNS = {
        'location_address' : 'div[class*=restoranAdres]::text',
        'arrive_time' : 'div[class*=servisSuresi]::text',
        'food_name' : 'span a[class*=productName]::text',
        'food_price' : 'span[class*=pv_new]::text',
        'food_description' : 'span[class*=productDescription]::text'
    }


class YSLocationPatterns:
    URL = [
        "http://istanbul.yemeksepeti.com/TR/Restoranlar/Bolge/kadikoy-%28merkez%29/e0b46c42-6718-4c7c-95be-208e835acd6a/tc/k.ys",
    ]
    MAIN_PATTERN = 'option'
    ITEM_PATTERNS = {
        'location_name' : 'option::text',
        'location_hash_id' : 'option::attr(value)'
    }