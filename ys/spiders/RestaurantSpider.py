#-*- coding: utf-8 -*-
__author__ = 'onurbaran'
from scrapy.spider import Spider
from scrapy.selector import Selector

from ys.items import YSRestaurantItem
from ys.spiders import YSRestaurantPatterns
class RestaurantSpider(Spider):
    name = "ys-restaurant-spider"
    allowed_domains = ["yemeksepeti.com"]
    start_urls = YSRestaurantPatterns.URL

    def parse(self, response):
        sel = Selector(response)
        """sel.css('div[class*=rmd_item]::attr(restaurantname)').extract()"""
        sites = sel.css(YSRestaurantPatterns.MAIN_PATTERN)
        items = []
        for site in sites:
            #@TODO: Generate names list from items.py
            item = YSRestaurantItem()
            item['restaurant_name'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['restaurant_name']).extract()
            item['restaurant_category_id'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['restaurant_category_id']).extract()
            item['paymentmethods'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['payment_methods']).extract()
            item['minprice'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['min_price']).extract()
            item['working_hours'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['working_hours']).extract()
            item['is_open'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['is_open']).extract()
            item['local_id'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['local_id']).extract()
            item['restaurant_link'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['restaurant_link']).extract()
            item['speed_rating'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['speed_rating']).extract()
            item['service_rating'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['service_rating']).extract()
            item['flavour_rating'] = site.css(YSRestaurantPatterns.ITEM_PATTERNS['flavour_rating']).extract()
            items.append(item)
        return items
