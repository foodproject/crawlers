#-*- coding: utf-8 -*-
__author__ = 'onurbaran'
from scrapy.spider import Spider
from scrapy.selector import Selector
from ys.spiders import YSLocationPatterns
from ys.items import YSLocationItem

class RestaurantSpider(Spider):
    name = "ys-location-spider"
    allowed_domains = ["yemeksepeti.com"]
    start_urls = YSLocationPatterns.URL

    def parse(self, response):
        sel = Selector(response)
        sites = sel.css(YSLocationPatterns.MAIN_PATTERN)
        items = []
        for site in sites:
            item = YSLocationItem()
            item['location_name'] = site.css(YSLocationPatterns.ITEM_PATTERNS['location_name']).extract()
            item['location_hash_id'] = site.css(YSLocationPatterns.ITEM_PATTERNS['location_hash_id']).extract()
            # @TODO: prepare for other spider's urls.
            item['location_url'] = None
            items.append(item)
        return items
